set nocompatible

syntax on 

set path=**
set wildmenu
set mouse=a
set backspace=indent,eol,start
set laststatus=2
set ttimeoutlen=0
set clipboard=unnamedplus 
set nofoldenable
set nobackup
set number
set ruler
set cursorline

set hlsearch
set incsearch
set showmatch
set ignorecase
set smartcase

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent

filetype off 

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'jnurmine/Zenburn'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/syntastic'

call vundle#end() 

filetype plugin indent on  

colorscheme zenburn
hi Normal ctermbg=None
let g:airline_powerline_fonts=1
let g:airline_theme='zenburn'

let g:netrw_banner=0
let g:netrw_liststyle=3

let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#tab_nr_type=1
let g:airline#extensions#tabline#show_buffers=0
let g:airline#extensions#tabline#tab_min_count=2

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
